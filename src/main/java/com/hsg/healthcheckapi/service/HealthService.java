package com.hsg.healthcheckapi.service;

import com.hsg.healthcheckapi.entity.Health;
import com.hsg.healthcheckapi.model.HealthItem;
import com.hsg.healthcheckapi.model.HealthRequest;
import com.hsg.healthcheckapi.model.HealthResponse;
import com.hsg.healthcheckapi.model.HealthStatusChangeRequest;
import com.hsg.healthcheckapi.repository.HealthRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class HealthService {
    private final HealthRepository healthRepository;

    public void setHealth(HealthRequest request) {
        Health addData = new Health();
        addData.setDateCreate(LocalDate.now());
        addData.setName(request.getName());
        addData.setHealthStatus(request.getHealthStatus());
        addData.setIsChronicDisease(request.getIsChronicDisease());
        addData.setEtcMemo(request.getEtcMemo());

        healthRepository.save(addData);
    }

    public List<HealthItem> getHealths() {
        List<Health> originList = healthRepository.findAll();

        List<HealthItem> result = new LinkedList<>();

        for (Health health : originList) { //향상된 for문
            HealthItem addItem = new HealthItem();
            addItem.setId(health.getId());
            addItem.setDateCreate(health.getDateCreate());
            addItem.setName(health.getName());
            addItem.setHealthStatus(health.getHealthStatus().getName());
            addItem.setIsGoHome(health.getHealthStatus().getIsGoHome());

            result.add(addItem);
        }

        return result;
    }

    public HealthResponse getHealth(long id) {
        Health originData = healthRepository.findById(id).orElseThrow();

        HealthResponse response = new HealthResponse();

        response.setId(originData.getId());

        response.setHealthStatusName(originData.getHealthStatus().getName());

//        if (originData.getHealthStatus().getIsGoHome()) {
//            response.setIsGoHomeName("예");
//        } else {
//            response.setIsGoHomeName("아니오");
//        }

        // 삼항연산자 => 간략한 if문
        response.setIsGoHomeName(originData.getHealthStatus().getIsGoHome() ? "예" : "아니오");

        response.setDateCreate(originData.getDateCreate());

        response.setName(originData.getName());

        response.setIsChronicDiseaseName(originData.getIsChronicDisease() ? "예" : "아니오");

        response.setEtcMemo(originData.getEtcMemo());

        return response;
    }

    public void putHealthStatus(long id, HealthStatusChangeRequest request) {
        Health originData = healthRepository.findById(id).orElseThrow();
        originData.setHealthStatus(request.getHealthStatus());

        healthRepository.save(originData);
    }

    public void delHealth(long id) {
        healthRepository.deleteById(id);
    }
}
