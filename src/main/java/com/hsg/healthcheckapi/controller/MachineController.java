package com.hsg.healthcheckapi.controller;

import com.hsg.healthcheckapi.model.MachineRequest;
import com.hsg.healthcheckapi.model.MachineStaticsResponse;
import com.hsg.healthcheckapi.service.MachineService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/machine")
public class MachineController {
    private final MachineService machineService;

    @PostMapping("/new")
    public String setMachine(@RequestBody MachineRequest request) {
        machineService.setMachine(request);

        return "OKK";
    }

    @GetMapping("/statics")
    public MachineStaticsResponse getStatics() {
        return machineService.getStatics();
    }
}
