package com.hsg.healthcheckapi.controller;

import com.hsg.healthcheckapi.model.HealthItem;
import com.hsg.healthcheckapi.model.HealthRequest;
import com.hsg.healthcheckapi.model.HealthResponse;
import com.hsg.healthcheckapi.model.HealthStatusChangeRequest;
import com.hsg.healthcheckapi.service.HealthService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/health")
public class HealthController {
    private final HealthService healthService;

    @PostMapping("/people")
    public String setHealth(@RequestBody HealthRequest request) {
        healthService.setHealth(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<HealthItem> getHealths() {
        return healthService.getHealths();
    }

    @GetMapping("/detail/{id}")
    public HealthResponse getHealth(@PathVariable long id) {
        return healthService.getHealth(id);
    }

    @PutMapping("/status/{id}")
    public String putHealthStatus(@PathVariable long id, @RequestBody HealthStatusChangeRequest request) {
        healthService.putHealthStatus(id, request);

        return "OKK";
    }

    @DeleteMapping("{id}")
    public String delHealth(@PathVariable long id) {
        healthService.delHealth(id);

        return "OKK";
    }

}
