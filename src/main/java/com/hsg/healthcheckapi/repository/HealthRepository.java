package com.hsg.healthcheckapi.repository;

import com.hsg.healthcheckapi.entity.Health;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HealthRepository extends JpaRepository<Health, Long> {
}
