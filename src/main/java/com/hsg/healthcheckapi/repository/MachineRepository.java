package com.hsg.healthcheckapi.repository;

import com.hsg.healthcheckapi.entity.Machine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MachineRepository extends JpaRepository<Machine, Long> {
}
