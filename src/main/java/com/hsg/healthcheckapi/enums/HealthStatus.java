package com.hsg.healthcheckapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum HealthStatus {
    FEEL_GOOD("좋음", false),
    NORMAL("보통", false),
    SEEK("나쁨", true),
    BE_DEAD("매우 나쁨", true);

    private final String name;
    private final Boolean isGoHome;
}
